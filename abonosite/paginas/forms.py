from django import forms
from paginas.models import Contacto
from paginas.models import Post
from paginas.models import Suscripcion


class ContactoForm(forms.ModelForm):
    correo = forms.EmailField(required=True)
    nombre = forms.CharField(required=True)
    mensaje = forms.CharField(widget=forms.Textarea, required=True)

    class Meta:
        model = Contacto
        fields = ('nombre','correo','mensaje')



class PostForm(forms.ModelForm):
    post = forms.CharField()
    email = forms.CharField()
    class Meta:
        model = Post
        fields = ('post','email')



class SuscripcionForm(forms.ModelForm):
    nombre = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = Suscripcion
        fields = ('email', )
