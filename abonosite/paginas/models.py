from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from tinymce import models as tinymce_models
from multiselectfield import MultiSelectField
# Create your models here.

class Funcion(models.Model):
    nombre = models.CharField( max_length=100, blank=True, null=True)
    imagen = models.CharField( max_length=100, blank=True, null=True)
    publico = models.BooleanField(default=False)
    descripcion = models.TextField( max_length=200, blank=True, null=True)

    def __str__(self):
        return self.nombre





class Nosotros(models.Model):
    titulo = models.CharField( max_length=100, blank=True, null=True)
    parrafo_1= models.CharField( max_length=100, blank=True, null=True)
    descripcion = models.TextField( max_length=200, blank=True, null=True)

    def __str__(self):
        return self.titulo

class Parte_planta(models.Model):
    pass
    nombre_parte = models.CharField( max_length=100, blank=True, null=True)
    coordenadas = models.CharField(max_length=200, blank=True, null=True)


    def __str__(self):
        return self.nombre_parte


class Etapa(models.Model):
    pass
    nombre = models.CharField( max_length=100, blank=True, null=True)
    imagen = models.CharField( max_length=100, blank=True, null=True)
    partes_plantas = models.ManyToManyField(Parte_planta)


    def __str__(self):
        return self.nombre

class Producto(models.Model):
    pass

    funcion = models.ManyToManyField(Funcion,)
    nombre = models.CharField( max_length=100, blank=True, null=True)
    descripcion = models.TextField(blank=True)
    descripcion2 = models.TextField(blank=True)
    imagen = models.CharField( max_length=100, blank=True, null=True)
    video = models.CharField(max_length=200, blank=True )
    propiedades = models.CharField(blank=True, max_length=100)
    publico = models.BooleanField(default=False)
    ficha_tecnica = models.CharField(max_length=100, blank=True, null=True)
    etapas = models.ManyToManyField(Etapa)
    slugs = models.SlugField(max_length=140, unique=True)



    def __str__(self):
        return self.nombre

    def _get_unique_slug(self):
        slugs = slugify(self.title)
        unique_slug = slugs
        num = 1
        while Article.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slugs, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slugs:
            self.slugs = self._get_unique_slug()
        super().save(*args, **kwargs)








class Cultivo(models.Model):
    pass
    nombre = models.CharField( max_length=100, blank=True, null=True)
    imagen = models.CharField( max_length=100, blank=True, null=True)
    descripcion = models.TextField( max_length=200, blank=True, null=True)
    publico = models.BooleanField(default=False)
    productos =  models.ManyToManyField(Producto)
    etapas = models.ManyToManyField(Etapa)




    def __str__(self):
        return self.nombre






MY_CHOICES = (('item_key1', 'Item title 1.1'),
              ('item_key2', 'Item title 1.2'),
              ('item_key3', 'Item title 1.3'),
              ('item_key4', 'Item title 1.4'),
              ('item_key5', 'Item title 1.5'))






class Carrusel(models.Model):
    imagen = models.CharField( max_length=100, blank=True, null=True)
    nombre = models.CharField(blank=True, max_length=100)
    descripcion = models.CharField(max_length=100)
    boton = models.CharField(blank=True, max_length=20)
    url_boton = models.CharField(blank=True, max_length=100)
    publico = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre

class Lineas(models.Model):

    nombre = models.CharField( max_length=100, blank=True, null=True)
    imagen = models.CharField( max_length=100, blank=True, null=True)
    descripcion = models.TextField(blank=True)
    propiedades = models.TextField(blank=True)
    publico = models.BooleanField(default=False)
    color = models.CharField( max_length=8, default="#000000")
    productos = models.ManyToManyField(Producto)

    def __str__(self):
        return self.nombre


class Linea(models.Model):

    nombre = models.CharField( max_length=100, blank=True, null=True)
    imagen = models.CharField( max_length=100, blank=True, null=True)
    descripcion = models.TextField(blank=True)
    propiedades = models.TextField(blank=True)
    publico = models.BooleanField(default=False)
    color = models.CharField( max_length=8, default="#000000")
    productos = models.ManyToManyField(Producto)

    def __str__(self):
        return self.nombre


class Contacto(models.Model):
    correo = models.EmailField()
    nombre = models.CharField(max_length=20)
    mensaje = models.TextField(blank=True)

    def __str__(self):
        return self.nombre


class Post(models.Model):
    post = models.CharField(max_length=500)
    email = models.CharField(max_length=500)




    def __str__(self):
        return self.post

class Suscripcion(models.Model):
    email = models.CharField(max_length=500)
    nombre = models.CharField(max_length=500)



    def __str__(self):
        return self.email
