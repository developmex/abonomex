# Generated by Django 2.0.7 on 2019-01-26 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0046_linea'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contacto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post', models.CharField(max_length=500)),
            ],
        ),
    ]
