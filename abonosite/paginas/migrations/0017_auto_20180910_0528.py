# Generated by Django 2.0.7 on 2018-09-10 05:28

from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0016_nosotros'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producto',
            name='cultivos',
        ),
        migrations.AddField(
            model_name='producto',
            name='cultivos',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('tomate', 'tomate'), ('calabaza', 'calabaza'), ('remolacha', 'remolacha')], default=0, max_length=25),
            preserve_default=False,
        ),
    ]
