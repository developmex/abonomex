# Generated by Django 2.0.7 on 2018-11-26 08:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0040_auto_20181115_2127'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='etapas',
            field=models.ManyToManyField(to='paginas.Etapa'),
        ),
    ]
