# Generated by Django 2.0.7 on 2018-11-10 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0029_auto_20181110_0556'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producto',
            name='cultivos',
        ),
        migrations.AddField(
            model_name='cultivo',
            name='producto',
            field=models.ManyToManyField(to='paginas.Producto'),
        ),
    ]
