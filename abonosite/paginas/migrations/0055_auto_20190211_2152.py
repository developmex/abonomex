# Generated by Django 2.0.7 on 2019-02-11 21:52

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0054_suscripcion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='descripcion',
            field=tinymce.models.HTMLField(),
        ),
    ]
