# Generated by Django 2.0.7 on 2018-11-15 21:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0038_parte_planta'),
    ]

    operations = [
        migrations.RenameField(
            model_name='parte_planta',
            old_name='imagen',
            new_name='nombre_parte',
        ),
        migrations.RemoveField(
            model_name='parte_planta',
            name='nombre',
        ),
        migrations.AddField(
            model_name='cultivo',
            name='partes_plantas',
            field=models.ManyToManyField(to='paginas.Parte_planta'),
        ),
    ]
