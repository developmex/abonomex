# Generated by Django 2.0.7 on 2018-08-03 10:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0003_carrusel_publico'),
    ]

    operations = [
        migrations.AddField(
            model_name='carrusel',
            name='nombre',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
