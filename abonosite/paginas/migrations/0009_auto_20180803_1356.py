# Generated by Django 2.0.7 on 2018-08-03 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paginas', '0008_auto_20180803_1323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carrusel',
            name='imagen',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='funcion',
            name='imagen',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='linea',
            name='imagen',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='imagen',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
