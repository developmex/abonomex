from django.contrib import admin

# Register your models here.
from . import models

class LineaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion',)
    filter_horizontal = ['productos',]
admin.site.register(models.Linea, LineaAdmin)




class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion',)
    filter_horizontal = ['funcion','etapas',]
admin.site.register(models.Producto, ProductoAdmin )

class CultivoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion',)
    filter_horizontal = ['etapas','productos']
admin.site.register(models.Cultivo, CultivoAdmin )

class EtapaAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    filter_horizontal = ['partes_plantas']





admin.site.register(models.Suscripcion)

admin.site.register(models.Contacto,)
admin.site.register(models.Etapa, EtapaAdmin )

admin.site.register(models.Funcion)
