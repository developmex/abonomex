from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic import ListView
from django.views.generic import DetailView
from django.utils import timezone
from paginas.models import Producto
from paginas.models import Carrusel
from paginas.models import Funcion
from paginas.models import Linea
from paginas.models import Nosotros
from paginas.models import Cultivo
from paginas.models import Etapa
from paginas.forms import ContactoForm
from paginas.forms import PostForm
from paginas.forms import SuscripcionForm
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views.generic import TemplateView



class LineaList(ListView):
        model = Linea



class LineaDetailView(DetailView):
        model = Linea








class EtapaList(ListView):
        model = Etapa
        def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
            context = super().get_context_data(**kwargs)
            # Add in a QuerySet of all the books
            cultivos = Cultivo.objects.all()
            etapa = Etapa.objects.all()
            context['etapa'] = etapa
            context['cultivos'] = cultivos


            return context




class CultivoList(ListView):
        model = Cultivo

        def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
            context = super().get_context_data(**kwargs)
            # Add in a QuerySet of all the books

            productos = Producto.objects.all()
            context['productos'] = productos


            return context


class CultivoDetailView(DetailView):
        model = Cultivo





        def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
            context = super().get_context_data(**kwargs)
            # Add in a QuerySet of all the books

            productos = Producto.objects.all()
            context['productos'] = productos


            return context


class FuncionList(ListView):
        model = Funcion

class FuncionDetailView(DetailView):
        model = Funcion


class ProductoList(ListView):
        queryset = Producto.objects.filter(publico=True)

        def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
            context = super().get_context_data(**kwargs)
            # Add in a QuerySet of all the books
            cultivos = Cultivo.objects.all()
            etapa = Etapa.objects.all()
            form = ContactoForm()

            linea_de_productos = Linea.objects.filter(publico=True)
            funcion = Funcion.objects.filter(publico=True)
            context['etapa'] = etapa
            context['cultivos'] = cultivos
            context['linea_de_productos'] = linea_de_productos
            context['funcion'] = funcion
            context['form'] = form

            return context






class ProductoDetailView(DetailView):

        model = Producto

        def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
            context = super().get_context_data(**kwargs)

            form = ContactoForm()

            context['form'] = form

            return context



class Inicio(TemplateView):
    template_name = "paginas/index.html"

    def get(self, request, *args, **kwargs):
        form = ContactoForm()
        cultivos = Producto.objects.all()
        elemento =  Carrusel.objects.filter(publico=True)
        productos = Producto.objects.filter(publico=True)
        linea_de_productos = Linea.objects.filter(publico=True)
        funcion = Funcion.objects.filter(publico=True)

        context = {'linea': 'linea',
                    'form': form,
                    'elemento': elemento,
                    'productos' : productos,
                    'funcion': funcion,
                    'linea_de_productos': linea_de_productos
        }

        return render(request, self.template_name  , context=context )


    def post(self, request):
        if request.method == 'POST':
            if request.POST.get("form_type") == 'contacto':
                form = ContactoForm(request.POST)
                if form.is_valid():
                    nombre = form.save(commit=False)
                    text = form.cleaned_data['nombre']

                    nombre.save()
                    print(text)
                    return redirect('inicio')

                args = {'form':form, 'text':text}
                return render(request, self.template_name, args)

            elif request.POST.get("form_type") == 'suscripcion':
                form = SuscripcionForm(request.POST)
                if form.is_valid():
                    nombre = form.save(commit=False)
                    text = form.cleaned_data['nombre']

                    nombre.save()
                    print(text)
                    return redirect('productos')

                args = {'form':form, 'text':text}
                return render(request, self.template_name, args)

        def subs(self, request):
            if request.method == 'POST':
                form = SuscripcionForm(request.POST)
                if form.is_valid():
                    nombre = form.save(commit=False)
                    text = form.cleaned_data['nombre']
                    nombre.save()
                    print(text)
                    return redirect('inicio')

                args = {'form':form, 'text':text}
                return render(request, self.template_name, args)





class Inicio2(View):

    def get(self, request, *args, **kwargs):
        cultivos = Producto.objects.all()
        elemento =  Carrusel.objects.filter(publico=True)
        linea_de_productos = Linea.objects.filter(publico=True)
        funcion = Funcion.objects.filter(publico=True)
        template = "paginas/main2.html"
        context = {'linea': 'linea',
                    'elemento': elemento,

                    'funcion': funcion,
                    'linea_de_productos': linea_de_productos
        }

        return render(request, template  , context=context )


class Inicio3(View):

    def get(self, request, *args, **kwargs):
        cultivos = Producto.objects.all()
        elemento =  Carrusel.objects.filter(publico=True)
        linea_de_productos = Linea.objects.filter(publico=True)
        funcion = Funcion.objects.filter(publico=True)
        template = "paginas/seg/index.html"
        context = {'linea': 'linea',
                    'elemento': elemento,

                    'funcion': funcion,
                    'linea_de_productos': linea_de_productos
        }

        return render(request, template  , context=context )




class Nosotros(View):

    def get(self, request, *args, **kwargs):

        template = "paginas/nosotros.html"

        return render(request, template  ,)

class Animacion(View):


    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        cultivos = Cultivo.objects.all()
        etapas = Etapa.objects.all()
        template = "paginas/demo1.html"
        context = {
        'productos' : productos,
        'cultivos': cultivos,
        'etapas': etapas,
        }

        return render(request, template, context=context)


class Demo2(View):


    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        cultivos = Cultivo.objects.all()
        etapas = Etapa.objects.all()
        template = "paginas/demo2.html"
        context = {
        'productos' : productos,
        'cultivos': cultivos,
        'etapas': etapas,
        }

        return render(request, template, context=context)




class Cultivo2(View):


    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        cultivos = Cultivo.objects.all()
        etapas = Etapa.objects.all()
        template = "paginas/cultivo2.html"
        context = {
        'productos' : productos,
        'cultivos': cultivos,
        'etapas': etapas,
        }

        return render(request, template, context=context)


class Cultivo3(View):


    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        cultivos = Cultivo.objects.all()
        etapas = Etapa.objects.all()
        template = "paginas/cultivo3.html"
        context = {
        'productos' : productos,
        'cultivos': cultivos,
        'etapas': etapas,
        }

        return render(request, template, context=context)


class Demo3(View):


    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        cultivos = Cultivo.objects.all()
        etapas = Etapa.objects.all()
        template = "paginas/demo3.html"
        context = {
        'productos' : productos,
        'cultivos': cultivos,
        'etapas': etapas,
        }

        return render(request, template, context=context)


class PostView(TemplateView):
    template_name = "paginas/post.html"

    def get(self, request):
        form = PostForm()
        return render(request, self.template_name  , {'form': form} )

    def post(self, request):
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            text = form.cleaned_data['post']

            post.save()
            print(text)
            return redirect('inicio')

        args = {'form':form, 'text':text}
        return render(request, self.template_name, args)


class ContactoView(TemplateView):
    template_name = "paginas/contacto.html"

    def get(self, request):
        form = ContactoForm()
        return render(request, self.template_name  , {'form': form} )

    def post(self, request):
        form = ContactoForm(request.POST)
        if form.is_valid():
            nombre = form.save(commit=False)
            text = form.cleaned_data['nombre']

            nombre.save()
            print(text)
            return redirect('inicio')

        args = {'form':form, 'text':text}
        return render(request, self.template_name, args)



class SuscripcionView(TemplateView):
    template_name = "paginas/suscripcion.html"

    def get(self, request):
        form = SuscripcionForm()
        return render(request, self.template_name  , {'form': form} )

    def post(self, request):
        form = SuscripcionForm(request.POST)
        if form.is_valid():
            email = form.save(commit=False)
            text = form.cleaned_data['email']

            email.save()
            print(text)
            return redirect('inicio')

        args = {'form':form,}
        return render(request, self.template_name, args)
