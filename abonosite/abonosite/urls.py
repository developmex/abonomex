"""abonosite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

from paginas.views import Producto

from paginas.views import Inicio
from paginas.views import Nosotros
from paginas.views import ProductoList
from paginas.views import LineaList
from paginas.views import Cultivo
from paginas.views import Cultivo2
from paginas.views import Cultivo3
from paginas.views import  Demo2
from paginas.views import Demo3
from paginas.views import EtapaList
from paginas.views import Animacion
from paginas.views import CultivoList
from paginas.views import CultivoDetailView
from paginas.views import FuncionList
from paginas.views import FuncionDetailView
from paginas.views import ProductoDetailView
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from paginas.views import LineaDetailView
from paginas.views import Inicio2
from paginas.views import Inicio3
from paginas.views import PostView
from paginas.views import ContactoView
from paginas.views import SuscripcionView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('inicio2', Inicio2.as_view(), name='inicio2'),
    path('inicio3', Inicio3.as_view(), name='inicio3'),
    path('', Inicio.as_view(), name='inicio'),
    path('contactos/', include('sendmail.urls')),
    path('productos/<int:pk>/', ProductoDetailView.as_view(), name='producto_detail'),
    path('funcion/', FuncionList.as_view(), name='funcion_list' ),
    path('funcion/<int:pk>/', FuncionDetailView.as_view(), name='funcion_detail'),
    path('linea-de-producto/', LineaList.as_view(), name='linea_list'),
    path('productos/', ProductoList.as_view(), name='producto_list' ),
    path('linea-de-producto/<int:pk>/', LineaDetailView.as_view(), name='linea_detail'),
    path('quienes-somos/', Nosotros.as_view(), name='nosotros'),
    path('cultivos/<int:pk>/', CultivoDetailView.as_view(), name='cultivo_detail'),
    path('nosotros/', Nosotros.as_view(), name='nosotros'),
    path('animacion/', Animacion.as_view(), name='animacion'),
    path('demo2/', Demo2.as_view(), name='demo2'),
    path('demo3/', Demo3.as_view(), name='demo3'),
    path('cultivos/',  Demo2.as_view(), name='demo2'),
    path('cultivos2/',  Cultivo2.as_view(), name='cultivo2'),
    path('cultivos3/',  Cultivo3.as_view(), name='cultivo3'),
    path('etapas/', EtapaList.as_view(), name='etapas'),
    path('post/', PostView.as_view(), name='post'),
    path('contacto/', ContactoView.as_view(), name='contacto'),
    path('suscripcion/', SuscripcionView.as_view(), name='suscripcion'),
    path('tinymce/', include('tinymce.urls')),





] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

app_name = "abonomex";
admin.site.site_header = 'Abonomex'
admin.site.site_title = 'Abonomex Panel'
