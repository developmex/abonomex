import sys, os
INTERP = "/home/abonomexweb/abonomex/dreamenv/bin/python3"
#INTERP is present twice so that the new python interpreter 
#knows the actual executable path 
if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)

cwd = os.getcwd()
sys.path.append(cwd)
sys.path.append(cwd + '/abonosite')  #You must add your project here

sys.path.insert(0,cwd+'/dreamenv/bin')
sys.path.insert(0,cwd+'/dreamenv/lib/python3.5/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = "abonosite.settings"
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
